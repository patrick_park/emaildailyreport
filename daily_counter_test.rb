#!/usr/bin/env ruby

require 'gmail'
require 'mysql2'
require 'spreadsheet'

Spreadsheet.client_encoding = 'UTF-8'

module Counter
  class DailyCounter
    DB_FRONT_INFO = {:host=>'172.27.11.74', :port=>3306, :username=>'root', :password=>'rnrnrn', :database=>'menu', :reconnect=>true, :as=>:hash}

    def connectMysql(db)
      return Mysql2::Client.new(db)
    end

    def querySql_front(query)
      begin
        if(!@client)
          @client = connectMysql(DB_FRONT_INFO)
        end
        return @client.query(query)
      rescue Exception => e
        puts e
        sleep(5)
        if(@client)
          @client.close
          @client = nil
        end
        retry
      end
    end

    def send_mail(file_name1, file_name2)
      d = Date.parse((Time.now-24*60*60).to_s)
      username = "sawook@leevi.co.kr"
      password = "tkdnr123"

      gmail = Gmail.new(username, password)
      gmail.deliver do
        #to "sodemian@naver.com,kyunso1004@naver.com,maesttrodh@naver.com,minchunjae@naver.com,ahnjimong@naver.com,apairo123@naver.com,yohaym@gmail.com,cameroncrazie@hotmail.com,jeijei12@leevi.co.kr,patrick@leevi.co.kr"
	#to "jeijei12@leevi.co.kr, sawook@leevi.co.kr"
	to "sawook@leevi.co.kr, cordiallys@gmail.com"
	subject "[리비] #{d.year}년 #{d.month}월 #{d.day}일 데이터 보내드립니다"
	html_part do
	  content_type 'text/html; charset=UTF-8'
	  body "<p>안녕하세요 리비 홍사욱입니다</p>
                <p></p>
		<p>#{d.year}년 #{d.month}월 #{d.day}일 데이터 보내드립니다</p>
		<p></p>
		<p>감사합니다<br>
		홍사욱 드림</p>"
	end
      add_file "/root/#{file_name1}"
      add_file "/root/#{file_name2}"
      end
    end

    def write_xls(type)
      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => '야구단'
      sheet2 = book.create_worksheet :name => '야구감독'
      sheet3 = book.create_worksheet :name => '야구선수'
      sheet5 = book.create_worksheet :name => '연예인'
      sheet6 = book.create_worksheet :name => '아이언'
      sheet7 = book.create_worksheet :name => '드라이버' 
      sheet8 = book.create_worksheet :name => '상품권' 

      sheet1.row(0).push "키워드", "뉴스", "SNS", "댓글"
      sheet2.row(0).push "키워드", "뉴스", "SNS", "댓글"
      sheet3.row(0).push "키워드", "뉴스", "SNS", "댓글"
      sheet5.row(0).push "키워드", "뉴스", "SNS", "댓글"
      sheet6.row(0).push "키워드", "뉴스", "SNS", "댓글"
      sheet7.row(0).push "키워드", "뉴스", "SNS", "댓글" 
      sheet8.row(0).push "키워드", "뉴스", "SNS", "댓글" 
      
      check_count_type1(sheet1, sheet2, sheet3, sheet5, sheet6, sheet7, sheet8) if type == 1
      check_count_type2(sheet1, sheet2, sheet3, sheet5, sheet6, sheet7, sheet8) if type == 2

      data_format = Spreadsheet::Format.new({ :size => 11 })
      book.default_format = data_format

      file_name = "daily_count_" + Date.parse((Time.now - 24*60*60).to_s).to_s + ".xls" if type == 1
      file_name = "daily_new_count_" + Date.parse((Time.now - 24*60*60).to_s).to_s + ".xls" if type == 2

      book.write "/root/#{file_name}"
      puts file_name

      return file_name
    end
  
    def push_row(data, sheet)
      n = 1
      data.each do |row|
        sheet.row(n).push "#{row['root_word']}".chomp(' (hyu-set)'), row["뉴스"], row["SNS"], row["댓글"]
        n += 1
      end
    end

    def check_count_type1(sheet1, sheet2, sheet3, sheet5, sheet6, sheet7, sheet8)
      start_date =  (Date.parse((Time.now - 24*60*60).to_s).to_s + " 00:00:00")
      end_date =  (Date.parse((Time.now - 24*60*60).to_s).to_s + " 23:59:59")
      puts "start_date = #{start_date}, end_date = #{end_date}"
      query = "SET @start_date = '#{start_date}'"
      querySql_front(query)
      query = "SET @end_date = '#{end_date}'"
      querySql_front(query)

      #야구구단
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news',  1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (168300,168301,168302,168303,168304,168305,168306,168307,168308,168309)
      GROUP BY dw.root_idx ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet1)

      #감독
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (168310,168311,168312,168313,168314,168315,170011,168317,168318,168319)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet2)

      #야구선수 
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news',  1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media  AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx AND ins.source_idx IN (11, 21, 1001, 28, 14, 3, 29, 130, 91, 40, 3004, 1150, 4, 6, 10, 1147, 5, 2, 19, 35, 1, 25, 106, 970, 30, 322, 17, 183, 238, 135, 131, 31, 71, 32, 78, 26, 1142, 89, 44, 77, 62, 104, 84, 182, 38, 64, 76, 46, 79, 74, 110, 16, 1152, 1403)
      #WHERE dw.root_idx IN (168320,168324,168325,168523,171312,171313,171314,171361,171362,168328,171315,171316,171317,168332,168333,168334,171318,171319,168323,171320,171321,168336,168341,168344,171322,171323,171324,168346,168347,168348,168349,168352,168353,171325,171326,168355,168356,168357,171327,171328,171329,168326,168358,168359,168363,171330,171331,171332,168364,168365,168366,168368,168899,171334,171363,171364,168342,168354,171504,171335,171336,171337,171338,168329,168340,168375,168376,168377,168378,168379,168327,168351)
      WHERE dw.root_idx IN (315185,317520,325495,301198,330541,331338,331339,324202,313296,311039,331340,329079,331341,320046,307011,328824,329020,329087,308745,329031,328108,310967,301836,325200,329092,331342,329325,314949,328072,328308,314069,307976,322066,331336,331337,300257,327041,311665,328664,328611,329095,311659,313617,315115,307043,330580,328703,331343,301496,306028,321708,317935,329364,330835,312004,310637,328888,312903,331437,329070,331344,328674,331345,314905,319338,307632,300610,325581,316191,313978,308584,306418, 331489)
      GROUP BY dw.root_idx ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet3)

      #연예인
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx AND ins.source_idx IN (11, 21, 1001, 28, 14, 3, 29, 130, 91, 40, 3004, 1150, 4, 6, 10, 1147, 5, 2, 19, 35, 1, 25, 106, 970, 30, 322, 17, 183, 238, 135, 131, 31, 71, 32, 78, 26, 1142, 89, 44, 77, 62, 104, 84, 182, 38, 64, 76, 46, 79, 74, 110, 16, 1152, 1403)
      WHERE 
      #dw.root_idx IN (168395,168396,168397,168398,171482,171483,171484,171485,171486,171487)
      dw.root_idx IN ('313015','312194','154229','150212','153502','150074','156833','311446','330411','326498')
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet5)

      #아이언
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (171522,171523,171524,171525,171526,171527,171528)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet6)

      #드라이버
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx IN (171529,171530,171531,171532,171533,171534,171535,171536)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet7)

      #상품권
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx IN (171537,171538,171539,171540,171541)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet8)
    end

    def check_count_type2(sheet1, sheet2, sheet3, sheet5, sheet6, sheet7, sheet8)
      start_date =  (Date.parse((Time.now - 24*60*60).to_s).to_s + " 00:00:00")
      end_date =  (Date.parse((Time.now - 24*60*60).to_s).to_s + " 23:59:59")
      puts "start_date = #{start_date}, end_date = #{end_date}"
      query = "SET @start_date = '#{start_date}'"
      querySql_front(query)
      query = "SET @end_date = '#{end_date}'"
      querySql_front(query)

      #야구구단
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news' AND mm.semantic_sign = 10,  1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (168300,168301,168302,168303,168304,168305,168306,168307,168308,168309)
      GROUP BY dw.root_idx ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet1)

      #감독
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (168310,168311,168312,168313,168314,168315,170011,168317,168318,168319)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet2)

      #야구선수 
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news' AND mm.semantic_sign = 10,  1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media  AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx AND ins.source_idx IN (11, 21, 1001, 28, 14, 3, 29, 130, 91, 40, 3004, 1150, 4, 6, 10, 1147, 5, 2, 19, 35, 1, 25, 106, 970, 30, 322, 17, 183, 238, 135, 131, 31, 71, 32, 78, 26, 1142, 89, 44, 77, 62, 104, 84, 182, 38, 64, 76, 46, 79, 74, 110, 16, 1152, 1403)
      #WHERE dw.root_idx IN (168320,168324,168325,168523,171312,171313,171314,171361,171362,168328,171315,171316,171317,168332,168333,168334,171318,171319,168323,171320,171321,168336,168341,168344,171322,171323,171324,168346,168347,168348,168349,168352,168353,171325,171326,168355,168356,168357,171327,171328,171329,168326,168358,168359,168363,171330,171331,171332,168364,168365,168366,168368,168899,171334,171363,171364,168342,168354,171504,171335,171336,171337,171338,168329,168340,168375,168376,168377,168378,168379,168327,168351)
      WHERE dw.root_idx IN (315185,317520,325495,301198,330541,331338,331339,324202,313296,311039,331340,329079,331341,320046,307011,328824,329020,329087,308745,329031,328108,310967,301836,325200,329092,331342,329325,314949,328072,328308,314069,307976,322066,331336,331337,300257,327041,311665,328664,328611,329095,311659,313617,315115,307043,330580,328703,331343,301496,306028,321708,317935,329364,330835,312004,310637,328888,312903,331437,329070,331344,328674,331345,314905,319338,307632,300610,325581,316191,313978,308584,306418, 331489)
      GROUP BY dw.root_idx ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet3)

      #연예인
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm
      ON dw.root_idx = mm.menu_idx  AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx AND ins.source_idx IN (11, 21, 1001, 28, 14, 3, 29, 130, 91, 40, 3004, 1150, 4, 6, 10, 1147, 5, 2, 19, 35, 1, 25, 106, 970, 30, 322, 17, 183, 238, 135, 131, 31, 71, 32, 78, 26, 1142, 89, 44, 77, 62, 104, 84, 182, 38, 64, 76, 46, 79, 74, 110, 16, 1152, 1403)
      WHERE 
      #dw.root_idx IN (168395,168396,168397,168398,171482,171483,171484,171485,171486,171487)
      dw.root_idx IN ('313015','312194','154229','150212','153502','150074','156833','311446','330411','326498')
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet5)

      #아이언
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx  IN (171522,171523,171524,171525,171526,171527,171528)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet6)

      #드라이버
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx IN (171529,171530,171531,171532,171533,171534,171535,171536)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet7)

      #상품권
      query = "SELECT dw.root_word, IFNULL(SUM(IF(category_code = 'news', 1, 0)),0) AS '뉴스', IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0) AS 'SNS',IFNULL(SUM(reply_cnt),0) AS '댓글'
      FROM dictionary.word_root AS dw
      LEFT JOIN  menu.menu_media AS mm 
      ON dw.root_idx = mm.menu_idx AND mm.write_date_media BETWEEN @start_date AND @end_date
      LEFT JOIN menu.menu_document AS md
      ON md.document_idx = mm.document_idx
      LEFT JOIN infra.source AS ins
      ON ins.source_idx = md.source_idx
      WHERE 
      dw.root_idx IN (171537,171538,171539,171540,171541)
      GROUP BY dw.root_idx
      ORDER BY (IFNULL(SUM(IF(category_code = 'news', 1, 0)),0)+IFNULL(SUM(IF(category_code = 'sns', 1, 0)),0)+IFNULL(SUM(reply_cnt),0)) DESC"
      data = querySql_front(query)
      push_row(data, sheet8)
    end

    def run
      file_name1 = write_xls(1)
      file_name2 = write_xls(2)

      send_mail(file_name1, file_name2)
      
      File.delete("/root/#{file_name1}")
      File.delete("/root/#{file_name2}")
    end

    if __FILE__ == $0
      DailyCounter.new.run
    end
  end
end

